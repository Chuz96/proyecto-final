console.log("Cargando Accordion...");
const dataAccordion = [{
    "title": "¿Acerca de nuestra tienda?",
    "desc": "Desde el 2009 De Moda viste hombres en Costa Rica. Contamos con talla regular y talla plus hasta 3XL, puedes encontrar ropa ejecutiva, casual y deportiva. En nuestra tienda online puedes comprar: camisas, abrigos, pantalones, accesorios,shorts entre otros."
},
{
    "title": "¿Condiciones de Garantia?",
    "desc": "Productos usados o lavados no se cambian,Productos en descuento o promoción no se cambian,El producto debe mantener las etiquetas originales y conservar la factura original."
},
{
    "title": "¿Tienda Fisica?",
    "desc": "Contamos con una tienda fisica ubicada en Alajuela Centro 100 metros norte de Pollo Macho."
},
{
    "title": "¿Como se aplican las ofertas?",
    "desc": "Las ofertas son aplicables en productos pagados a contado, dependiendo del monto mas descuento aplicara la oferta."
}];


(function (){
    let ACCORDION ={
        init: function (){
            let _self = this;
            //llamanos las funciones
            this.insertData(_self);
            this.eventHundler(_self);
        },

        eventHundler: function (_self){
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++){
                arrayRefs[x].addEventListener('click', function(event){
                    console.log('event', event);
                    _self.showTab(event.target);
                });
            }
        },
 showTab: function(refItem){
     let activeTab = document.querySelector('.tab-active');

     if(activeTab){
         activeTab.classList.remove('tab-active');
     }

    
     console.log('show tab', refItem);
     refItem.parentElement.classList.toggle('tab-active');
     },

insertData: function (_self){
    dataAccordion.map(function(item, index) {
        //consola.log
    document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));
    });
},
tplAccordionItem: function (item){
    return(`<div class='accordion-item'>
    <p class= 'accordion-title'> ${item.title}</p>
    <p class= 'accordion-desc'> ${item.desc}</p>
    </div>`)},
 }
 ACCORDION.init();
 })();
 