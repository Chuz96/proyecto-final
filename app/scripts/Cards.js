console.log("Cargando Cards...");
const dataCards =[{
    "title": "Camisas",
      "url_image":"https://www.dhresource.com/0x0/f2/albu/g5/M00/7A/24/rBVaI1mZEoiAMXupAAWV_FWx3b8745.jpg/2017-new-fashion-brand-men-shirt-pocket-fight.jpg",
      "desc":"Si lo que quieres es verte siempre diferente, este es el sitio que debes visitar, encontraras todo tipo de estilos, tallas y marcas todo a tu gusto.",
      "cta":"Show More",
      
},
{
    "title": "Pantalanoes",
      "url_image":"https://http2.mlstatic.com/D_NQ_NP_811296-MLA46076963622_052021-V.jpg",
      "desc":"Si lo que quieres es verte siempre diferente, este es el sitio que debes visitar, encontraras todo tipo de estilos, tallas y marcas todo a tu gusto.",
      "cta":"Show More",
    
 },
 {   
      "title": "Abrigos",
      "url_image":"https://i.pinimg.com/236x/72/3d/4a/723d4a80af69ff196b2a6b612961bfb6.jpg",
      "desc":"Si lo que quieres es verte siempre diferente, este es el sitio que debes visitar, encontraras todo tipo de estilos, tallas y marcas todo a tu gusto.",
      "cta":"Show More",
     
 },
 {
      "title": "Zapatos",
      "url_image":"https://cdn.coordiutil.com/imagen-bota_bosi-1920467-800-600-1-75.jpg",
      "desc":"Si lo que quieres es verte siempre diferente, este es el sitio que debes visitar, encontraras todo tipo de estilos, tallas y marcas todo a tu gusto.",
      "cta":"Show More",
     
 },
 {
      "title": "Accesorios",
      "url_image":"http://ae01.alicdn.com/kf/HTB1DsFnXyHrK1Rjy0Flq6AsaFXa8.jpg",
      "desc":"Si lo que quieres es verte siempre diferente, este es el sitio que debes visitar, encontraras todo tipo de billeteras, reloj y gorras todo a tu gusto.",
      "cta":"Show More",
     
 },
{
      "title": "Ofertas",
      "url_image":"https://www.latercera.com/resizer/HmgZ2_ahB5fgEuqJZzA4yvpFdDw=/900x600/smart/cloudfront-us-east-1.images.arcpublishing.com/copesa/KASR2XOVA5EAPNIGCQN4KLHISE.jpg",
      "desc":"Buscas poder ahorar un poco de efectivo, pero siempre estar a la moda este es el apartado perfecto para ti, podras encontrar todo tipo de ofertas en todos los productos de la tienda.",
      "cta":"Show More",
     

}];

(function (){
    let CARD = {
        init: function () {
            //console.log('card module was loaded');
       let _self = this;
       
       //llamamos funciones
       this.insertData(_self);
       //this.eventHandler(_self);
     },
     eventHandler: function (_self){
         let arrayRefs = document.querySelectorAll('.accordion-title');

         for (let x = 0; x <arrayRefs.length; x++){
             arrayRefs[x].addEventListener('click', function(event){
            console.log('event', event);
            _self.showTab(event.target);
             });
            }
        },

insertData: function(_self){
    dataCards.map(function (item, index){
        document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
    });
    },


tplCardItem: function(item, index){
    return(`<div class='card-item' id="card-number-${index}">
    <img src="${item.url_image}"/>
    <div class="card-info">
    <p class='card-title'>${item.title}</p>
    <p class='card-desc'>${item.desc}</p>
    <p class='card-cta' target="blank" href="${item.link}">${item.cta}</a>
    </div>
    </div>`)}, 
}
    CARD.init();
         
     
    })();
